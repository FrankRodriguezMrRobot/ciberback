<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'database.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// GET DATA FORM REQUEST
$data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg['message'] = '';

// CHECK IF RECEIVED DATA FROM THE REQUEST
if(isset($data->anunciante) && isset($data->precio) && isset($data->telefono) && isset($data->incluyeHab) && isset($data->descripcion) && isset($data->imagen)){
    // CHECK DATA VALUE IS EMPTY OR NOT
    if(!empty($data->anunciante) && !empty($data->precio) && !empty($data->telefono) && !empty($data->incluyeHab) && !empty($data->descripcion) && !empty($data->imagen)){
        
        $insert_query = "INSERT INTO `posts`(anunciante,precio,telefono,incluyeHab,descripcion,imagen) VALUES(:anunciante,:precio,:telefono,:incluyeHab,:descripcion,:imagen)";
        
        $insert_stmt = $conn->prepare($insert_query);
        // DATA BINDING
        $insert_stmt->bindValue(':anunciante', htmlspecialchars(strip_tags($data->anunciante)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':precio', htmlspecialchars(strip_tags($data->precio)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':telefono', htmlspecialchars(strip_tags($data->telefono)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':incluyeHab', htmlspecialchars(strip_tags($data->incluyeHab)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':descripcion', htmlspecialchars(strip_tags($data->descripcion)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':imagen', htmlspecialchars(strip_tags($data->imagen)),PDO::PARAM_STR);
        
        if($insert_stmt->execute()){
            $msg['message'] = 'Data Inserted Successfully';
        }else{
            $msg['message'] = 'Data not Inserted';
        } 
        
    }else{
        $msg['message'] = 'Oops! empty field detected. Please fill all the fields';
    }
}
else{
    $msg['message'] = 'Please fill all the fields | title, body, author';
}
//ECHO DATA IN JSON FORMAT
echo  json_encode($msg);
?>